<img src="activemq logo.png" alt="ActiveMQ-Logo" height="70px" /> <img src="logo_spring.jpg" alt="spring-Logo" height="70px" />

# activeMQ-spring-sandbox

This is a web-based spring-based application, that uses Spring's JMS in order to send and receive messages from ActiveMQ.

The relevant maven dependencies are:

```xml
<dependency>
	<groupId>org.springframework</groupId>
	<artifactId>spring-jms</artifactId>
	<version>${ohadr.spring.version}</version>
</dependency>

<dependency>
	<groupId>org.apache.activemq</groupId>
	<artifactId>activemq-pool</artifactId>
	<version>5.15.3</version>
</dependency>

<dependency>
	<groupId>org.apache.activemq</groupId>
	<artifactId>activemq-spring</artifactId>
	<version>5.8.0</version>
</dependency>
```

the XML configuration is:

```xml
<amq:connectionFactory id="amqConnectionFactory"
	brokerURL="${jms.url}" 
	userName="${jms.username}"
	password="${jms.password}" />
```

where `jms.utl` is configurable, and is set in config.properties to:

	jms.url=tcp://localhost:61616

## `QueueSender`

is a wrapper for JmsTemplate. For example, the controller that sends a message uses this object.

In version 1.1-Snapshot, I have added an API for delete queue.

## `QueueListener`

There is a spring bean, `QueueListener`, that is registered to the same Queue and read the messages. It is defined also in the spring-XML to act as the listener. **If you want to send messages without to immediately read them, the best approach is to comment-out the relevant lines in the spring-XML file**. 

## How to Run?

from command line, use the following command:

	mvn clean tomcat7:run -Dohadr.project.port=8093

Note that 'ohadr.project.port' is a property that lets you set the port that the application will be listening to. If none is set, the default port is 8080. You can use a different port that 8093, of course.

The application exposes Web-API for sending a message and placing it on the Queue:
	
	GET /send HTTP/1.1
		message=<message>
		
There is a registered listener that immediately gets notified, and polls the message from the Queue.

## Debug within Eclipse

if you wish to run this web-application in debug mode using Eclipse, the debug configuration described in the image below:

![debug_within_eclipse](debug_within_eclipse.jpg)


## Running Multiple Instances 

The genius thing to do is to run multiple instances of the application (allowed by the configurable port), send messages and see how the listeners polls it from the Queue. For example, you can open a command line and run the application from port 8092:  

	mvn tomcat7:run -Dohadr.project.port=8092

then open another command line, and run another instance from port 8093:

	mvn tomcat7:run -Dohadr.project.port=8093
	
![bla](run_multiple.jpg)


now open 2 tabs in Chrome, and type:

	http://localhost:8092/activemq-spring-sandbox/send?message=ohad from 8092

and 

	http://localhost:8093/activemq-spring-sandbox/send?message=ohad from 8093

note the different ports, for each tab.
In the command line (or the logger, if configured), you will see how the listener polls the messages. Of course, the 8093 listener can poll the message that was sent by 8092. 

## ActiveMQ console

If the listener does not poll the message from the Queue immediately (as it does), the message can be seen in the admin-console of ActiveMQ:

![activemq_admin_console](activemq_admin_console.jpg)

and if you click on the specific queue, you will see the message:

![activemq_admin_console_message](activemq_admin_console_message.jpg)

Note the Queue name (Queue.Name) which is set in config.properties.

## References

This project is based on the below articles:

Good article on the subject is:
http://activemq.apache.org/spring-support.html

There is a link there to this article, "Efficient Lightweight JMS with Spring and ActiveMQ (2009)":
https://medium.com/@bdarfler/efficient-lightweight-jms-with-spring-and-activemq-51ff6a135946

# Concurrency

From docs:

> The number of concurrent sessions/consumers to start for each listener. Can either be a simple number indicating the maximum number (e.g. "5") or a range indicating the lower as well as the upper limit (e.g. "3-5"). Note that a specified minimum is just a hint and might be ignored at runtime. Default is 1; keep concurrency limited to 1 in case of a topic listener or if message ordering is important; consider raising it for general queues.

To see concurrency in action (and understand how it works), I have added to the `send()` Controller a new parameter: amount. So you can call 'send' with the message you want to send to the Queue, and the amount of times you want this message to be sent. For example:

	http://.../send?message=myMessage&amount=25
	
in this case, 25 messages will be sent: myMessage#0, myMessage#1, myMessage#2 on so on till myMessage#24. 

The concurrency can be controlled by the `concurrency` property in the `jms:listener-container`, in the XML configuration: 

    <jms:listener-container concurrency="2">...
In this example, we limit the concurrency to 2 consumers.

When we want to allow more consumers, e.g. 10, we can configure:

    <jms:listener-container concurrency="10">...
Note that in this case the max number of consumers will be 10, but probably we will see less consumers in the admin console. If we want to force 10 consumers, we can configure:

    <jms:listener-container concurrency="10-10">...
This force ActiveMQ to start with 10 consumers, but not more than 10. This because we configured a range from 10 to 10. When 10-10 is configured, the admin console looks right after start:

![activemq_admin_console](activemq_admin_console 10-10.jpg)

Then, when we call send and dispatch 60 messages, by:

    http://.../send?message=ohadsss&amount=60
    
we see that ActiveMQ sends each consumer equal number of messages:
    
![activemq_admin_console](activemq_admin_console 10-10_in_action.jpg)

## References - Concurrency

https://stackoverflow.com/questions/33339224/spring-jms-listener-container-concurrency-attribute-not-working
